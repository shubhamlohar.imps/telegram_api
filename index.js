const dotenv = require('dotenv')
dotenv.config()
const TelegramBot = require('node-telegram-bot-api');
const express = require('express');
const app = express();
const cors = require('cors');
const Joi = require('joi');
const port = process.env.PORT || 3000;

app.use(express.urlencoded({limit: '200mb', extended: false}));
app.use(express.json({ limit: '200mb' }))
app.use(cors())

// replace the value below with the Telegram token you receive from @BotFather
const token = process.env.TOKEN;

// Create a bot that uses 'polling' to fetch new updates
const bot = new TelegramBot(token, {polling: true});

function validateMediaData(data)
{
    const JoiSchema = Joi.object({
        id: Joi.string().required(),
        mimetype: Joi.string().required(),
        base64: Joi.string().required(),
        filename: Joi.string().required(),
    }).options({ abortEarly: false });

    return JoiSchema.validate(data)
}

app.post('/sendMedia', (req, res) => {
    try {
        const response = validateMediaData(req.body)
        if(response.error)
        {  
            res.send(response.error.details) 
            return
        }
        
        const { id, mimetype, base64, filename } = req.body;
        const fileOptions = {
            // Explicitly specify the file name.
            filename,
            // Explicitly specify the MIME type.
            contentType: mimetype,
          };
        var data = Buffer.from(base64, 'base64');
        bot.sendDocument(id, data, {}, fileOptions);
        res.send('ok')
    } catch (error) {
        console.log('error', error)
        res.send(error)
    }
})

function validateMessageData(data)
{
    const JoiSchema = Joi.object({
        id: Joi.string().required(),
        message: Joi.string().required(),
    }).options({ abortEarly: false });

    return JoiSchema.validate(data)
}

app.post('/sendMessage', (req, res) => {
    try {
        const response = validateMessageData(req.body)
        if(response.error)
        {  
            res.send(response.error.details) 
            return
        }
        
        const { id, message } = req.body;
        bot.sendMessage(id, message);
        res.send('ok')
    } catch (error) {
        console.log('error', error)
        res.send(error)
    }
})

app.use('/status', (_, res) => res.send('Backend is Working'))
app.use('*', (_, res) => res.send('API NOT FOUND'))

// Matches "/echo [whatever]"
bot.onText(/\/id/, (msg, match) => {

  const chatId = msg.chat.id;
  
  if(chatId){
      bot.sendMessage(chatId, "Chat Id: `" + chatId + "`", {
        parse_mode: 'MarkdownV2'
      });
  }
});

// // Listen for any kind of message. There are different kinds of
// // messages.
// bot.on('message', (msg) => {
//   const chatId = msg.chat.id;

//   // send a message to the chat acknowledging receipt of their message
//   bot.sendMessage(chatId, 'Received your message');
// });

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})